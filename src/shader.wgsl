const NUMBER = 7;

struct Uniform {
    pos: vec2<f32>,
    col: vec3<f32>,
}

@group(0) @binding(0) var<uniform> uni: array<Uniform, NUMBER>;

@vertex 
fn vs_main( @builtin(vertex_index) VertexIndex : u32 ) 
-> @builtin(position) vec4<f32> 
{
    var pos = array<vec2<f32>, 6>(
        vec2<f32>( -1.0,  1.0),
        vec2<f32>( 1.0, 1.0),
        vec2<f32>( -1.0, -1.0),
        vec2<f32>( -1.0, -1.0),
        vec2<f32>( 1.0, 1.0),
        vec2<f32>( 1.0, -1.0),
    );

    return vec4<f32>(pos[VertexIndex], 0.0, 1.0);
}

@fragment 
fn fs_main( @builtin(position) in: vec4<f32> ) -> @location(0) vec4<f32> { 
    var dist = 1000000.0;
    var index = 0;

    for (var i = 0; i < NUMBER; i++) {
        let pt = uni[i].pos;
        let d = distance(pt.xy, in.xy);

        if (d < dist) {
            dist = d;
            index = i;
        }
    }

    var out = uni[index].col;

    if dist < 10.0 {
        out = vec3<f32>(1.0,1.0,1.0);
    }

    return vec4<f32>(out,1.0);
}
