const std = @import("std");
const core = @import("core");
const gpu = core.gpu;

pub const App = @This();

const Rng = std.rand.DefaultPrng;

const VELOCITY = 2;
const PARTICLE_NUMBER = 7;

title_timer: core.Timer,
pipeline: *gpu.RenderPipeline,
bind_group: *gpu.BindGroup,
particles: [PARTICLE_NUMBER]Particle,
buffer: *gpu.Buffer,
pause: bool = false,

const Particle = struct {
    pos: [2]f32,
    vel: [2]f32,
    col: [3]f32,
    _: u32 = 0,
};

fn createParticles(max: core.Size) [PARTICLE_NUMBER]Particle {
    var rng = Rng.init(69420);
    const rand = rng.random();

    var particles: [PARTICLE_NUMBER]Particle = undefined;

    for (&particles) |*p| {
        const x = rand.uintLessThan(u32, max.width);
        const y = rand.uintLessThan(u32, max.height);

        p.pos[0] = @as(f32, @floatFromInt(x));
        p.pos[1] = @as(f32, @floatFromInt(y));
        p.vel[0] = VELOCITY;
        p.vel[1] = VELOCITY;
        p.col[0] = rand.float(f32);
        p.col[1] = rand.float(f32);
        p.col[2] = rand.float(f32);
    }

    return particles;
}

fn moveParticles(particles: []Particle, max: core.Size) void {
    for (particles) |*p| {
        const max_x = @as(f32, @floatFromInt(max.width));
        const max_y = @as(f32, @floatFromInt(max.height));

        if (p.pos[0] <= 0) p.vel[0] *= -1;
        if (p.pos[1] <= 0) p.vel[1] *= -1;
        if (p.pos[0] >= max_x) p.vel[0] *= -1;
        if (p.pos[1] >= max_y) p.vel[1] *= -1;

        p.pos[0] = std.math.clamp(p.pos[0] + p.vel[0], 0.0, max_x);
        p.pos[1] = std.math.clamp(p.pos[1] + p.vel[1], 0.0, max_y);
    }
}

pub fn init(app: *App) !void {
    try core.init(.{});

    const shader_module = core.device.createShaderModuleWGSL(
        "shader.wgsl",
        @embedFile("shader.wgsl"),
    );

    defer shader_module.release();

    // Fragment state
    const fragment = gpu.FragmentState.init(.{
        .module = shader_module,
        .entry_point = "fs_main",
        .targets = &.{.{
            .format = .bgra8_unorm,
            .blend = &gpu.BlendState{
                .color = .{ .dst_factor = .one },
                .alpha = .{ .dst_factor = .one },
            },
            .write_mask = gpu.ColorWriteMaskFlags.all,
        }},
    });

    // Create Buffers
    const uniform_buffer = core.device.createBuffer(&.{
        .label = "Uniform Buffer",
        .usage = .{
            .uniform = true,
            .copy_dst = true,
        },
        .size = @sizeOf(Particle) * PARTICLE_NUMBER,
    });

    // Set up Bind Groups

    const uniform_layout_desc = gpu.BindGroupLayout.Descriptor.init(.{
        .label = "Uniform bing group layout",
        .entries = &[_]gpu.BindGroupLayout.Entry{
            .{
                .binding = 0,
                .visibility = .{ .fragment = true },
                .buffer = .{ .type = .uniform },
            },
        },
    });

    const uniform_layout = core.device.createBindGroupLayout(&uniform_layout_desc);

    const uniform_bind_desc = gpu.BindGroup.Descriptor.init(.{
        .label = "Uniform bind group",
        .layout = uniform_layout,
        .entries = &[_]gpu.BindGroup.Entry{
            .{
                .binding = 0,
                .buffer = uniform_buffer,
                .size = @sizeOf(Particle) * PARTICLE_NUMBER,
            },
        },
    });

    const uniform_bind_group = core.device.createBindGroup(&uniform_bind_desc);

    const layout_desc = gpu.PipelineLayout.Descriptor.init(.{
        .label = "Render Pipeline Layout",
        .bind_group_layouts = &[_]*gpu.BindGroupLayout{uniform_layout},
    });

    const layout = core.device.createPipelineLayout(&layout_desc);

    const pipeline = core.device.createRenderPipeline(&.{
        .fragment = &fragment,
        .layout = layout,
        .vertex = gpu.VertexState{
            .module = shader_module,
            .entry_point = "vs_main",
        },
    });

    app.* = .{
        .title_timer = try core.Timer.start(),
        .pipeline = pipeline,
        .bind_group = uniform_bind_group,
        .particles = createParticles(core.size()),
        .buffer = uniform_buffer,
    };
}

pub fn deinit(app: *App) void {
    defer core.deinit();
    _ = app;
}

pub fn update(app: *App) !bool {
    var iter = core.pollEvents();

    while (iter.next()) |event| {
        switch (event) {
            .close => return true,
            .key_press => |key| switch (key.key) {
                .q => return true,
                .space => app.pause = !app.pause,
                else => {},
            },
            else => {},
        }
    }

    if (!app.pause) moveParticles(&app.particles, core.size());
    core.queue.writeBuffer(app.buffer, 0, &app.particles);

    const back_buffer_view = core.swap_chain.getCurrentTextureView().?;
    const color_attachment = gpu.RenderPassColorAttachment{
        .view = back_buffer_view,
        .clear_value = std.mem.zeroes(gpu.Color),
        .load_op = .clear,
        .store_op = .store,
    };

    const encoder = core.device.createCommandEncoder(null);
    const render_pass_info = gpu.RenderPassDescriptor.init(.{
        .color_attachments = &.{color_attachment},
    });
    const pass = encoder.beginRenderPass(&render_pass_info);
    pass.setPipeline(app.pipeline);
    pass.setBindGroup(0, app.bind_group, null);
    pass.draw(6, 1, 0, 0);
    pass.end();
    pass.release();

    var command = encoder.finish(null);
    encoder.release();

    core.queue.submit(&[_]*gpu.CommandBuffer{command});
    command.release();
    core.swap_chain.present();
    back_buffer_view.release();

    // update the window title every second
    if (app.title_timer.read() >= 1.0) {
        app.title_timer.reset();
        try core.printTitle("Triangle [ {d}fps ] [ Input {d}hz ]", .{
            core.frameRate(),
            core.inputRate(),
        });
    }

    return false;
}
